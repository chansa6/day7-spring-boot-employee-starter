package com.thoughtworks.springbootemployee.service;

import com.thoughtworks.springbootemployee.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.thoughtworks.springbootemployee.Employee;

import java.util.List;

@Service
public class CompanyEmployeeService {
    private final EmployeeRepository employeeRepository;

    @Autowired
    public CompanyEmployeeService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    public List<Employee> getEmployeesInCompany(Long companyId) {
        return employeeRepository.findEmployeesByCompanyId(companyId);
    }
}
