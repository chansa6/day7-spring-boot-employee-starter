package com.thoughtworks.springbootemployee;

public class Employee {
    private Long id;
    private final String name;
    private Integer age;
    private final String gender;
    private Integer salary;
    private Long companyId;

    public Employee(Long id, String name, Integer age, String gender, Integer salary, Long companyId) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.gender = gender;
        this.salary = salary;
        this.companyId = companyId;
    }

    public Long getId() {
        return id;
    }

    public Integer getAge() {
        return age;
    }

    public String getGender() {
        return gender;
    }

    public Integer getSalary() {
        return salary;
    }

    public void setId(Long idToAdd) {
        this.id = idToAdd;
    }

    public void setSalary(Integer salary) {
        this.salary = salary;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public String getName() {
        return name;
    }
}
