package com.thoughtworks.springbootemployee.controller;

import com.thoughtworks.springbootemployee.Company;
import com.thoughtworks.springbootemployee.Employee;
import com.thoughtworks.springbootemployee.repository.CompanyRepository;
import com.thoughtworks.springbootemployee.service.CompanyEmployeeService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/companies")
public class CompanyController {
    private final CompanyRepository companyRepository;
    private final CompanyEmployeeService companyEmployeeService;

    public CompanyController(CompanyRepository companyRepository, CompanyEmployeeService companyEmployeeService) {
        this.companyRepository = companyRepository;
        this.companyEmployeeService = companyEmployeeService;
    }

    @GetMapping
    public List<Company> getAllCompanies() {
        return companyRepository.findAllCompanies();
    }

    @GetMapping("/{id}")
    public Company getSpecificCompany(@PathVariable Long id) {
        return companyRepository.findSpecificCompany(id);
    }

    @GetMapping("/{id}/employees")
    List<Employee> getEmployeesInCompany(@PathVariable Long id) {
        return companyEmployeeService.getEmployeesInCompany(id);
    }

    @GetMapping(params = {"pageNumber", "pageSize"})
    public List<Company> getCompaniesByPage(@RequestParam int pageNumber, @RequestParam int pageSize) {
        return companyRepository.findCompanyByPage(pageNumber, pageSize);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Company addCompany(@RequestBody Company company) {
        return companyRepository.addCompany(company);
    }

    @PutMapping("/{id}")
    public Company updateCompany(@RequestBody Company company, @PathVariable Long id) {
        return companyRepository.updateCompany(company, id);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteCompany(@PathVariable Long id) {
        companyRepository.deleteCompany(id);
    }

}
