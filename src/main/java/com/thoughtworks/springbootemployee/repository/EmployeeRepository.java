package com.thoughtworks.springbootemployee.repository;

import com.thoughtworks.springbootemployee.Employee;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class EmployeeRepository {
    private final List<Employee> employees;

    public EmployeeRepository() {
        this.employees = new ArrayList<>();
        employees.add(new Employee(1L, "Lucy", 20, "female", 8000, 1L));
        employees.add(new Employee(2L, "Lily", 31, "female", 8200, 1L));
        employees.add(new Employee(3L, "Marcus", 43, "male", 9000, 2L));
    }

    public List<Employee> findAllEmployees() {
        return employees;
    }

    public Employee findSpecificEmployee(Long targetId) {
        return this.employees.stream().filter(employee -> employee.getId().equals(targetId)).findFirst().orElse(null);
    }

    public List<Employee> findEmployeesByGender(String targetGender) {
        return this.employees.stream().filter(employee -> employee.getGender().equals(targetGender)).collect(Collectors.toList());
    }

    public List<Employee> findEmployeesByCompanyId(Long companyId) {
        return this.employees.stream().filter(employee -> employee.getCompanyId().equals(companyId)).collect(Collectors.toList());
    }

    public Employee addEmployee(Employee employee) {
        Long idToAdd = generateId();
        employee.setId(idToAdd);
        employees.add(employee);
        return employee;
    }

    private Long generateId() {
        return employees.stream().mapToLong(Employee::getId).max().orElse(0L) + 1;
    }

    public List<Employee> findByPage(int pageNumber, int pageSize) {
        return employees.stream()
                .skip((long) (pageNumber - 1) * pageSize)
                .limit(pageSize)
                .collect(Collectors.toList());
    }

    public Employee updateEmployee(Employee employee, Long id) {
        Employee employeeToUpdate = findSpecificEmployee(id);
        employeeToUpdate.setAge(employee.getAge());
        employeeToUpdate.setSalary(employee.getSalary());
        return employeeToUpdate;
    }

    public void deleteEmployee(Long id) {
        employees.remove(findSpecificEmployee(id));
    }

}
