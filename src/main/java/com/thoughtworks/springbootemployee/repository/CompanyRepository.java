package com.thoughtworks.springbootemployee.repository;


import com.thoughtworks.springbootemployee.Company;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class CompanyRepository {
    List<Company> companies;

    public CompanyRepository() {
        this.companies = new ArrayList<>();
        companies.add(new Company(1L, "spring"));
        companies.add(new Company(2L, "kmpg"));
        companies.add(new Company(3L, "ninecent"));
    }

    public List<Company> findAllCompanies() {
        return companies;
    }

    public Company findSpecificCompany(Long id) {
        return this.companies.stream().filter(company -> company.getId().equals(id)).findFirst().orElse(null);
    }

    public List<Company> findCompanyByPage(int pageNumber, int pageSize) {
        return companies.stream()
                .skip((long) (pageNumber - 1) * pageSize)
                .limit(pageSize)
                .collect(Collectors.toList());
    }

    public Company addCompany(Company company) {
        Long idToAdd = generateId();
        company.setId(idToAdd);
        companies.add(company);
        return company;
    }

    private Long generateId() {
        return companies.stream().mapToLong(Company::getId).max().orElse(0L) + 1;
    }

    public Company updateCompany(Company company, Long companyId) {
        Company companyToUpdate = findSpecificCompany(companyId);
        companyToUpdate.setName(company.getName());
        return companyToUpdate;
    }

    public void deleteCompany(Long id) {
        companies.remove(findSpecificCompany(id));
    }
}
